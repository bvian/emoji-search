import React from 'react';
import PropTypes from "prop-types";

import './Emoji.css';

class Emoji extends React.Component {

    static propTypes = {
        title: PropTypes.string,
        symbol: PropTypes.string
    };

    render() {
        return (
            <div
                className='emoji-card copy-to-clipboard'
                data-clipboard-text={this.props.symbol}
            >
                <div className='emoji'>{this.props.symbol}</div>
                <div className='title'>
                    <span>{this.props.title}</span>
                </div>
            </div>
        );
    }
}

export default Emoji;